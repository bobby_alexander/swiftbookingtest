﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using SwiftBookingTest.Web.Managers;
using SwiftBookingTest.Web.Models;

namespace SwiftBookingTest.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IClientManager _clientManager;

        //
        // GET: /Home/
        public HomeController(IClientManager clientManager)
        {
            _clientManager = clientManager ?? throw new ArgumentNullException("clientManager");
        }

        public ActionResult Index()
        {
            var clients = _clientManager.GetAllClients();

            return View(clients);
        }

        [HttpPost]
        public ActionResult AddClient(string name, string phone, string address)
        {
            _clientManager.AddClient(new Client {Name = name, Phone = phone, Address = address});
            var clients = _clientManager.GetAllClients();
            return View("~/Views/Home/Index.cshtml", clients);
        }

        [HttpPost]
        public async Task<ContentResult> CreateBooking(string clientId)
        {
            var resultJson = await _clientManager.CreateBookingForClient(int.Parse(clientId));

            return Content(resultJson,"application/json");
        }
    }
}