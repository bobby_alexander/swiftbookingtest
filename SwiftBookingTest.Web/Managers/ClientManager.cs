﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SwiftBookingTest.Web.Models;
using SwiftBookingTest.Web.Repository;

namespace SwiftBookingTest.Web.Managers
{
    public class ClientManager : IClientManager
    {
        private readonly ISwiftBookingManager _bookingManager;
        private readonly IClientRepository _repository;

        public ClientManager(IClientRepository clientRepository, ISwiftBookingManager bookingManager)
        {
            _repository = clientRepository;
            _bookingManager = bookingManager;
        }

        public void AddClient(Client newClient)
        {
            if (newClient != null)
                _repository.Add(newClient);
        }

        public void DeleteClient(int clientId)
        {
            if (clientId > 0)
                _repository.Delete(clientId);
        }

        public IEnumerable<Client> GetAllClients()
        {
            return _repository.FetchAll();
        }

        public Client GetClient(int clientId)
        {
            return _repository.Fetch(clientId);
        }

        public void UpdateClient(Client updatedClient)
        {
            if (updatedClient != null)
                _repository.Update(updatedClient.ClientId,
                    updatedClient);
        }

        public async Task<string> CreateBookingForClient(int clientId)
        {
            var client = GetClient(clientId);
            var result = await _bookingManager.BookDelivery(client.Address);
            return result;
        }
    }
}