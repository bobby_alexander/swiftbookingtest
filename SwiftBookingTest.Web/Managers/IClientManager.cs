﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SwiftBookingTest.Web.Models;

namespace SwiftBookingTest.Web.Managers
{
    public interface IClientManager
    {
        void AddClient(Client newClient);
        void UpdateClient(Client updatedClient);
        void DeleteClient(int clientId);
        Client GetClient(int clientId);

        IEnumerable<Client> GetAllClients();

        Task<string> CreateBookingForClient(int clientId);
    }
}