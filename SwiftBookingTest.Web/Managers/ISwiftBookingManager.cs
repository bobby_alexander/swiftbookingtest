using System.Threading.Tasks;

namespace SwiftBookingTest.Web.Managers
{
    public interface ISwiftBookingManager
    {
        Task<string> BookDelivery(string dropoffAddress);
    }
}