﻿using System.Threading.Tasks;
using Newtonsoft.Json;
using SwiftBookingTest.Web.Models;

namespace SwiftBookingTest.Web.Managers
{
    public class SwiftBookingManager : ISwiftBookingManager
    {
        //Should be ideally retrieved from an encrypted config and not hardcoded
        private const string MerchantKey = "3285db46-93d9-4c10-a708-c2795ae7872d";

        private readonly PickupDropOffDetail _defaultPickupAddress;

        public SwiftBookingManager()
        {
            _defaultPickupAddress = new PickupDropOffDetail {Address = "1/23 Flinders lane, Melbourne, Victoria 3000"};
        }

        public async Task<string> BookDelivery(string dropoffAddress)
        {
            var dropOffaddress = new PickupDropOffDetail {Address = dropoffAddress};
            var booking = new SwiftBooking
            {
                ApiKey = MerchantKey,
                Booking = new BookingDetails {PickupDetail = _defaultPickupAddress, DropOffDetail = dropOffaddress}
            };

            using (var client = new RestClient())
            {
                var result = await client.Post("api/v2/deliveries", booking);
                if (result.IsSuccessStatusCode)
                    return await result.Content.ReadAsStringAsync();
                return $"Error! Could not book delivery. {  await result.Content.ReadAsStringAsync()}";
            }
        }
    }
}