﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SwiftBookingTest.Web.Models
{
    public class ClientContext: DbContext
    {
        public ClientContext():base("ClientDB")
        {
            Database.SetInitializer<ClientContext>(new CreateDatabaseIfNotExists<ClientContext>());
        }

        public DbSet<Client> Clients { get; set; }
    }
}