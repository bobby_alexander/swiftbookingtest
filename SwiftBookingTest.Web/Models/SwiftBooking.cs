﻿namespace SwiftBookingTest.Web.Models
{
    public class SwiftBooking
    {
        public string ApiKey { get; set; }
        public BookingDetails Booking { get; set; }
    }

    public class PickupDropOffDetail
    {
        public string Address { get; set; }
    }

    public class BookingDetails
    {
        public PickupDropOffDetail PickupDetail { get; set; }
        public PickupDropOffDetail DropOffDetail { get; set; }
    }
}