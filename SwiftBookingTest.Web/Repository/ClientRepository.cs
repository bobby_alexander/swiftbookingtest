﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using SwiftBookingTest.Web.Models;

namespace SwiftBookingTest.Web.Repository
{
    public class ClientRepository : IClientRepository
    {
        private readonly object _updateLock = new object();
        private readonly ClientContext context = new ClientContext();

        public void Add(Client item)
        {
            context.Clients.Add(item);
            context.SaveChanges();
        }

        public Client Fetch(int id)
        {
            return context.Clients.Find(id);
        }

        public void Update(int id, Client updatedItem)
        {
            lock (_updateLock)
            {
                var original = context.Clients.Find(updatedItem.ClientId);
                if (original == null)
                    return; //Nothing to update. Ideally should throw a RecordNotFoundException and propogate back to the user
                var clientToUpdate = context.Clients.Attach(updatedItem);
                context.Entry(updatedItem).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            lock (_updateLock)
            {
                var client = context.Clients.Find(id);
                if (client == null)
                    return;
                context.Clients.Remove(client);
                context.SaveChanges();
            }
        }

        public IEnumerable<Client> FetchAll()
        {
            var clients = context.Clients.ToList();

            return clients;
        }
    }
}