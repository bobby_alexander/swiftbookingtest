﻿using SwiftBookingTest.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwiftBookingTest.Web.Repository
{
    public interface IClientRepository: IRepository<Client>
    {
    }
}
