﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwiftBookingTest.Web.Repository
{
    public interface IRepository<T>
    {
        void Add(T item);
        T Fetch(int id);
        void Update(int id, T updatedItem);
        void Delete(int id);

        IEnumerable<T> FetchAll();
        


    }
}
