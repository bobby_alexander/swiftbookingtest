﻿using System;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SwiftBookingTest.Web
{
    public class RestClient: IDisposable
    {
        private readonly HttpClient _client;


        public RestClient()
        {
            _client = new HttpClient {BaseAddress = new Uri("https://app.getswift.co/")};

            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<HttpResponseMessage> Post<T>(string endpoint, T postMessage)
        {
            return await _client.PostAsJsonAsync(endpoint, postMessage);
        
        }

        public void Dispose()
        {
            _client?.Dispose();
        }
    }
}