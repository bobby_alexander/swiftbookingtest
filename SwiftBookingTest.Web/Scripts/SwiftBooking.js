﻿<script type="text/javascript">
        $(function () {
            $("#createBookingButton").click(function () {
                $.ajax({
                    type: "POST",
                    url: "/Home/CreateBooking",
                    data: '{name: "' + $("#clientIdField").val() + '" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        alert("Response: " + response.responseText);
                    },
                    failure: function (response) {
                        alert(response.responseText);
                    },
                    error: function (response) {
                        alert(response.responseText);
                    }
                });
            });
        });
    </script>